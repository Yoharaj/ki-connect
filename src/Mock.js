export const MOCK_BC_PARTNERS = [
  {
    bcName: "Magalir",
    averageTicketSize: 347500.0,
    areaOfOperation: "Gujarat,Uttar Pradesh,Madhya Pradesh",
    searchMatchPercentage: 0.0,
    averageTAT: 100.0,
    kiRating: 2,
  },
  {
    bcName: "MSM",
    averageTicketSize: 975000.0,
    areaOfOperation: "Tamil Nadu,Gujarat,Madhya Pradesh,Uttar Pradesh",
    searchMatchPercentage: 0.0,
    averageTAT: 100.0,
    kiRating: 3,
  },
  {
    bcName: "Swasti",
    averageTicketSize: 1165000.0,
    areaOfOperation: "Tamil Nadu,Rajasthan,Assam,Jharkhand",
    searchMatchPercentage: 0.0,
    averageTAT: 100.0,
    kiRating: 4,
  },
  {
    bcName: "Subahlaxmi",
    averageTicketSize: 595000.0,
    areaOfOperation: "Tamil Nadu,Chattisgarh,Madhya Pradesh,Andhra Pradesh",
    searchMatchPercentage: 0.0,
    averageTAT: 100.0,
    kiRating: 1,
  },
];

export const MOCK_POOLS = [
  {
    poolName: "A",
    volume: 4.745e7,
    averageSeasoning: 14.61,
    searchMatchPercentage: 100.0,
    kiRating: 4,
  },
  {
    poolName: "B",
    volume: 3.702e7,
    averageSeasoning: 12.46,
    searchMatchPercentage: 100.0,
  },
  {
    poolName: "C",
    volume: 7.873e7,
    averageSeasoning: 17.09,
    searchMatchPercentage: 100.0,
  },
  {
    poolName: "D",
    volume: 4.538e7,
    averageSeasoning: 14.65,
    searchMatchPercentage: 100.0,
  },
];

export const BC_FILTER = [{ label: "Gender", options: [], parameter: "" }];

export const BC_MENU_TYPES = [
  "MFI Loans",
  "MEL Loans",
  "Two Wheeler Loans",
  "KCC Loans",
  "Tractor",
];

export const DA_MENU_TYPES = ["Mortgage Backed Securities", "Auto Loans"];
