import { MONTHS_FULLNAME } from "../constants";

export const getFormattedAddress = (address = "") => {
  if (!address) return "";
  let addressComponents = address.split(" ");
  if (addressComponents.length > 5) {
    return addressComponents.slice(addressComponents.length - 5).join(" ");
  }
  return addressComponents.join(" ");
};

const DATA_MAP = {
  credit: {},
};

export const formatMonthWiseData = (monthwiseData = null) => {
  if (!monthwiseData) return null;
  let monthData = Object.keys(monthwiseData) || [];
  let monthsObj = monthData.map((month) => {
    let response = {};
    response["title"] = "January-22";
    let d = month.split("-")[1];
    console.log(d.includes("01"), "month");
    //`${
    // MONTHS_FULLNAME[parseInt(month.split("-")[1]) - 1]
    // }-${month.split["-"][0]}`;
    response["keyValue"] = month;
    return response;
  });
  return monthsObj;
};

/*
export const formattedResponse = (data =MOCK_DATA) => {
  const allMonths = Object.keys(data);

  return data;
}
*/
