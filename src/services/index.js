import axios from "axios";
import { get } from "lodash";
import { store } from "../store";
import { updateReportData } from "../store/slices/ReportSlice";

export const analyseReport = async (file = null) => {
  if (!file) return null;
  const payload = new FormData();
  payload.append("file", file);
  try {
    const { data } = await axios.post(
      "http://192.168.1.143:8080/kiinsights/universal-analyzer/upload?bankName=ICICI",
      payload
    );
    const categoricalWiseData = get(data, "insightsDTO1", null);
    const monthwiseData = get(data, "insightsDTO2", null);
    const ds = get(data, "dsRes", null);

    store.dispatch(
      updateReportData({
        monthwiseData: monthwiseData,
        categoricalWiseData: categoricalWiseData,
        ds: ds,
      })
    );
    return data;
  } catch (e) {}
};

export const fetchLenders = () => {
  return axios
    .get("http://localhost:8091/kaleidofin-dashboard/api/lenders")
    .catch((error) => console.error(error));
};
export const fetchDAs = () => {
  return axios
    .get("http://localhost:8091/kaleidofin-dashboard/api/DA")
    .catch((error) => console.error(error));
};
