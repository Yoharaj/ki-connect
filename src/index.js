import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { createTheme, responsiveFontSizes, ThemeProvider } from "@mui/material";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { store } from "./store";

let theme = createTheme({
  palette: {
    primary: {
      light: "#28B2B6",
      main: "#28B2B6",
      dark: "#28B2B6",
      contrastText: "#fff",
    },
    secondary: {
      light: "#F4F6FA",
      main: "#ff753a",
      dark: "#002884",
      contrastText: "#fff",
    },
    typography: {
      allVariants: {
        fontFamily: `"HK Grotesk", sans-serif`,
      },
    },
    text: {
      fontFamily: `"HK Grotesk", sans-serif`,
    },
  },
});

theme = responsiveFontSizes(theme);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </Provider>
);
