import { Box, Grid } from "@mui/material";
import { get, isEmpty } from "lodash";
import React from "react";
import { useSelector } from "react-redux";
import {
  MOCK_MONTH_X_AXIS,
  MOCK_MONTH_Y_AXIS,
  MONTHS_FULLNAME,
} from "../constants";
import MonthwiseCard from "../molecules/MonthwiseCard";
import { formatMonthWiseData } from "../utils";

const MonthwiseAnalysis = () => {
  const monthwiseData = useSelector(({ report }) => report.monthwiseData) || {};
  if (isEmpty(monthwiseData)) return <></>;

  const generateMonthWiseCardData = () => {
    const monthDataMap = formatMonthWiseData(monthwiseData) || [];
    return monthDataMap.map((month, index) => (
      <Grid item lg={12}>
        <MonthwiseCard
          key={index}
          // title={get(, "title", "")}
          title={MONTHS_FULLNAME[index] + " - " + "22"}
          keyValue={get(month, "keyValue", null)}
          chartData={[
            {
              xAxis: MOCK_MONTH_X_AXIS,
              yAxis: MOCK_MONTH_Y_AXIS,
            },
          ]}
        />
      </Grid>
    ));
  };

  const data = generateMonthWiseCardData();
  return (
    <Box>
      <Grid container spacing={3} alignItems="center" justifyContent="center">
        {data}
      </Grid>
    </Box>
  );
};
export default MonthwiseAnalysis;

/*
  return (
    <Box>
      <Grid container spacing={3} alignItems="center" justifyContent="center">
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <MonthwiseCard
            title="January"
            chartData={[
              {
                xAxis: MOCK_MONTH_X_AXIS,
                yAxis: MOCK_MONTH_Y_AXIS,
              },
            ]}
          />
        </Grid>
      </Grid>
    </Box>
  );
};
*/
