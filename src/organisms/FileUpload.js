import { Button, Stack, Typography } from "@mui/material";
import axios from "axios";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { analyseReport } from "../services";
import { updateLoader } from "../store/slices/LoaderSlice";
import { updateReportData } from "../store/slices/ReportSlice";
import { updateToastMessage } from "../store/slices/ToastSlice";

const FileUpload = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const file = useSelector(({ report }) => report.file);

  const handleChange = async (e) => {
    const file = e.target.files[0];
    dispatch(
      updateReportData({
        file: file,
      })
    );
    dispatch(
      updateLoader({
        load: true,
        loaderText: "Uploading",
      })
    );

    const reports = analyseReport(file);

    await new Promise((resolve) =>
      setTimeout(() => {
        dispatch(updateLoader({ loaderText: "Analysing" }));
        resolve(true);
      }, 2000)
    );

    await new Promise((resolve) =>
      setTimeout(() => {
        dispatch(updateLoader({ loaderText: "Generating Reports" }));
        resolve(true);
      }, 1000)
    );

    await new Promise((resolve) =>
      setTimeout(() => {
        dispatch(updateLoader({ load: false, loaderText: "Done" }));
        dispatch(
          updateToastMessage({
            message: "Reports Generated Successfully.",
            severity: "success",
          })
        );
        navigate("/reports");
        resolve(true);
      }, 1000)
    );
  };

  return (
    <Stack alignItems="center" justifyContent="center">
      <Button component="label" fullWidth variant="contained">
        Upload
        <input
          type="file"
          accept="application/pdf"
          hidden
          onChange={handleChange}
        />
      </Button>
      <br />
      {file && <Typography>Selected File: {file.name} </Typography>}
    </Stack>
  );
};

export default FileUpload;
