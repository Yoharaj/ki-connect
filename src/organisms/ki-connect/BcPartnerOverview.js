import FilterListIcon from "@mui/icons-material/FilterList";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import { Box, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Filter from "../../molecules/ki-connect/Filter";
import LoanTypeMenuBar from "../../molecules/ki-connect/LoanTypeMenuBar";
import { updateLoader } from "../../store/slices/LoaderSlice";
import BcPartner from "./BcPartner";
import Pool from "./Pool";
import { fetchLenders } from "../../services";
import { get } from "lodash";
import { BC_MENU_TYPES } from "../../Mock";

const BcPartnerOverview = ({
  title = "",
  sectionTitle = "",
  data = [],
  isBcPartner = false,
}) => {
  const dispatch = useDispatch();
  const [bcPartner, setBcPartners] = React.useState([]);

  useEffect(() => {
    dispatch(
      updateLoader({
        load: true,
        loaderText: `Fetching ${title} data...`,
      })
    );
    fetchLenders().then((response) => {
      console.log(response);
      dispatch(
        updateLoader({
          load: false,
        })
      );
      setBcPartners(get(response, "data", []));
    });
  }, [title]);

  return (
    <Stack spacing={2}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "80%",
          }}
        >
          <Typography variant="h4" fontWeight={600}>
            {title}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-evenly",
            flexDirection: "row",
            width: "20%",
          }}
        >
          <Filter
            icon={<FilterListIcon />}
            title="Filter"
            list={["test", "tasd", "asds"]}
          />
          <Filter
            icon={<SwapVertIcon />}
            title="Sort"
            list={["test", "tasd", "asds"]}
          />
        </Box>
      </Box>
      <LoanTypeMenuBar loanTypes={BC_MENU_TYPES} />

      <Stack spacing={3}>
        {bcPartner.map((partner, index) => (
          <React.Fragment>
            {isBcPartner ? (
              <BcPartner
                data={partner}
                key={index}
                index={index}
                title={sectionTitle}
              />
            ) : (
              <Pool
                data={partner}
                key={index}
                index={index}
                title={sectionTitle}
              />
            )}
            <hr
              style={{
                margin: "auto",
                width: "100%",
                opacity: "10%",
              }}
            ></hr>
          </React.Fragment>
        ))}
      </Stack>
    </Stack>
  );
};

export default BcPartnerOverview;
