import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Paper,
  Rating,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect } from "react";
import RupeesDisplay from "../../atoms/ki-connect/RupeesDisplay";
import { COLOR_CODES } from "../../constants";

import magalir from "../../assets/partners/magalir.svg";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { get } from "lodash";

const Pool = ({ data = {}, index = 0, title = "" }) => {
  const isUserLoggedIn = useSelector(({ login }) =>
    get(login, "loggedIn", false)
  );
  const [parameters, setParameters] = React.useState([]);

  useEffect(() => {
    setParameters([
      <Box>
        <Typography>Volume</Typography>
        <Typography fontWeight={600}>{data?.volume}</Typography>
      </Box>,
      <Box>
        <Typography>Average Seasoning</Typography>
        <Typography fontWeight={600}>{data?.averageSeasoning}</Typography>
      </Box>,
      <Box>
        <Typography>Pool Availability</Typography>
        <Typography fontWeight={600}>{data?.poolAvailability || "--"}</Typography>
      </Box>,
      <Box>
        <Typography>Search Match Percentage</Typography>
        <Typography fontWeight={600}>{data?.searchMatchPercentage} &#37;</Typography>
      </Box>,
    ]);
  }, [data]);

  return (
    <Paper elevation={0} sx={{ padding: 2, borderRadius: 4 }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "self-start", gap: "15px" }}>
          <Avatar alt="Remy Sharp" src="" />
          <Stack spacing={1}>
            <Typography variant="h6" fontWeight={600}>
              {`${title} - ${isUserLoggedIn ? data?.poolName : index + 1}`}
            </Typography>
            <Stack alignItems='center' direction='row' spacing={5}>
              {parameters}
            </Stack>

            <Rating name="read-only" value={data?.kiRating || 2} readOnly />
          </Stack>
        </Box>
        {isUserLoggedIn ? (
          <Box>
            <Link to="/pool">
              <Button
                variant="contained"
                sx={{
                  backgroundColor: COLOR_CODES.light,
                  color: COLOR_CODES.secondary,
                  height:50,
                  width:175,
                  borderRadius: "15px",
                  "&:hover": {
                    backgroundColor: COLOR_CODES.light,
                    color: COLOR_CODES.secondary,
                  },
                }}
              >
                <Typography fontWeight={500} align="center">
                  View More
                </Typography>
              </Button>
            </Link>
          </Box>
        ) : (
          <></>
        )}
      </Box>
    </Paper>
  );
};

export default Pool;
