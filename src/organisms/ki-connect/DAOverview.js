import FilterListIcon from "@mui/icons-material/FilterList";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import { Box, Stack, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Filter from "../../molecules/ki-connect/Filter";
import LoanTypeMenuBar from "../../molecules/ki-connect/LoanTypeMenuBar";
import { updateLoader } from "../../store/slices/LoaderSlice";
import BcPartner from "./BcPartner";
import { fetchDAs } from "../../services";
import { get } from "lodash";
import Pool from "./Pool";
import { DA_MENU_TYPES } from "../../Mock";

const DAOverview = ({ title = "", sectionTitle = "", data = [] }) => {
  const dispatch = useDispatch();
  const [bcPartner, setBcPartners] = React.useState([]);

  useEffect(() => {
    dispatch(
      updateLoader({
        load: true,
        loaderText: `Fetching ${title} data...`,
      })
    );
    fetchDAs().then((response) => {
      console.log(response);
      dispatch(
        updateLoader({
          load: false,
        })
      );
      setBcPartners(get(response, "data", []));
    });
  }, [title]);

  return (
    <Stack spacing={2}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "80%",
          }}
        >
          <Typography variant="h4" fontWeight={600}>
            {title}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-evenly",
            flexDirection: "row",
            width: "20%",
          }}
        >
          <Filter
            icon={<FilterListIcon />}
            title="Filter"
            list={["test", "tasd", "asds"]}
          />
          <Filter
            icon={<SwapVertIcon />}
            title="Sort"
            list={["test", "tasd", "asds"]}
          />
        </Box>
      </Box>
      <LoanTypeMenuBar loanTypes={DA_MENU_TYPES} />
      <Stack spacing={3}>
        {bcPartner.map((partner, index) => (
          <React.Fragment>
            <Pool
              data={partner}
              key={index}
              index={index}
              title={sectionTitle}
            />
            <hr
              style={{
                margin: "auto",
                width: "100%",
                opacity: "10%",
              }}
            ></hr>
          </React.Fragment>
        ))}
      </Stack>
    </Stack>
  );
};

export default DAOverview;
