import {
  Box,
  Paper,
  Typography
} from "@mui/material";
import React from "react";

import { useSelector } from "react-redux";

const PoolDetails = () => {
  const [tabValue, setTabValue] = React.useState(0);

  const data = useSelector(({ data }) => data);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Paper elevation={1} sx={{ padding: 2, borderRadius: 4 }}>
      <Box>
        <Typography variant="h5" fontWeight={600} align="center">
          Pool A
        </Typography>
      </Box>
    </Paper>
  );
};

export default PoolDetails;
