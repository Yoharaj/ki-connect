import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Paper,
  Rating,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect } from "react";
import RupeesDisplay from "../../atoms/ki-connect/RupeesDisplay";
import { COLOR_CODES } from "../../constants";

import magalir from "../../assets/partners/magalir.svg";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { get } from "lodash";

const BcPartner = ({ data = {}, index = 0, title = "" }) => {
  const isUserLoggedIn = useSelector(({ login }) =>
    get(login, "loggedIn", false)
  );
  const [parameters, setParameters] = React.useState([]);

  useEffect(() => {
    setParameters([
      <Box>
        <Typography>Area of Operation</Typography>
        <Typography fontWeight={600}>
          {getAreaOfOperation(data?.areaOfOperation)}
        </Typography>
      </Box>,
      <RupeesDisplay amount={data?.averageTicketSize} />,
      <Box>
        <Typography>Average TAT</Typography>
        <Typography fontWeight={600}>{data?.averageTAT}</Typography>
      </Box>,
    ]);
  }, [data]);

  const getAreaOfOperation = (areas) => {
    console.log(areas);
    if (areas) {
      return areas.split(",")[0];
    }
    return "Not Disclosed";
  };
  return (
    <Paper elevation={0} sx={{ padding: 2, borderRadius: 4 }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "self-start", gap: "15px" }}>
          <Avatar alt="Remy Sharp" src="" />
          <Stack spacing={1}>
            <Typography variant="h6" fontWeight={600}>
              {isUserLoggedIn ? (
                <Typography>
                  BC Partner -
                  <Typography
                    variant="h5"
                    component="span"
                    fontWeight={600}
                    sx={{
                      color: "transparent",
                      textShadow: "0 0 20px rgba(0, 0, 0, 0.5)",
                    }}
                  >
                    {data?.bcName}
                  </Typography>
                </Typography>
              ) : (
                `${title} - ${index + 1}`
              )}
            </Typography>
            <Stack alignItems="center" direction="row" spacing={5}>
              {parameters}
            </Stack>

            <Rating
              sx={{
                "& .MuiRating-iconFilled": {
                  color: COLOR_CODES.primary,
                },
              }}
              name="read-only"
              value={data?.kiRating || 2}
              readOnly
              size="small"
              color="primary"
            />
          </Stack>
        </Box>
        {isUserLoggedIn ? <Box>
            <Button color="info">Get In Touch?</Button>
        </Box> : <></>}
        {isUserLoggedIn ? (
          <Box>
            <Link to="/bcPartner">
              <Button
                variant="contained"
                sx={{
                  backgroundColor: COLOR_CODES.light,
                  color: COLOR_CODES.secondary,
                  height: 50,
                  width: 175,
                  borderRadius: "15px",
                  "&:hover": {
                    backgroundColor: COLOR_CODES.light,
                    color: COLOR_CODES.secondary,
                  },
                }}
              >
                <Typography fontWeight={500} width={150} align="center">
                  View More
                </Typography>
              </Button>
            </Link>
          </Box>
        ) : (
          <></>
        )}
      </Box>
    </Paper>
  );
};

export default BcPartner;
