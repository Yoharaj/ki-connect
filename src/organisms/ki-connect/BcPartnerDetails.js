import {
    Box,
    Paper,
    Typography
} from "@mui/material";
import React from "react";

import { useSelector } from "react-redux";

const BcPartnerDetails = () => {
  const data = useSelector(({ data }) => data);

  return (
    <Paper elevation={1} sx={{ padding: 2, borderRadius: 4 }}>
      <Box>
        <Typography align="center">
          BC Partner -
          <Typography
            variant="h5"
            component="span"
            fontWeight={600}
            sx={{
              color: "transparent",
              textShadow: "0 0 20px rgba(0, 0, 0, 0.5)",
            }}
          >
            BC Partner
          </Typography>
        </Typography>
      </Box>
    </Paper>
  );
};

export default BcPartnerDetails;
