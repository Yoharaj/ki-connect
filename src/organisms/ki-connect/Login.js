import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { useDispatch, useSelector } from "react-redux";
import { get } from "lodash";
import { updateLoginState } from "../../store/slices/LoginSlice";
import logo from "../../assets/logo.svg";
import { updateLoader } from "../../store/slices/LoaderSlice";
import { updateAlert } from "../../store/slices/AlertsSlice";

const Login = () => {
  const dispatch = useDispatch();
  const open = useSelector(({ login }) => get(login, "openLogin", false));

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    console.log("Login clicked");
    dispatch(
      updateLoader({
        load: true,
        loaderText: "Please wait",
      })
    );
    setTimeout(() => {
      dispatch(
        updateLoader({
          load: false,
        })
      );
      dispatch(
        updateLoginState({
          openLogin: false,
          loggedIn: true,
          name: "Samasta",
        })
      );
      dispatch(
        updateAlert({
          open: true,
          message: "Logged In Successfully",
          severity: "success",
        })
      );
      handleClose();
    }, 2000);
  };

  const handleClose = () => {
    dispatch(
      updateLoginState({
        openLogin: false,
      })
    );
  };

  return (
    <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
      <DialogTitle>
        <Box textAlign="center">
          <img src={logo} alt="Logo" />
          <Typography variant="h4">Login</Typography>
        </Box>
      </DialogTitle>
      <DialogContent>
        <Box textAlign="center">
          <TextField
            label="Email"
            type="email"
            fullWidth
            margin="normal"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            label="Password"
            type="password"
            fullWidth
            margin="normal"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button
            variant="contained"
            fullWidth
            color="primary"
            onClick={handleLogin}
            size="large"
            disabled={!email || !password}
          >
            Login
          </Button>
          <br />
          <br />
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default Login;
