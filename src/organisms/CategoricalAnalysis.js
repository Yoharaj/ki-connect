import { Box, Grid } from "@mui/material";
import React from "react";
import { MOCK_Y_AXIS, MOCK_Y_AXIS_AMOUNT, MONTHS, TRANSACTIONS, TRANSACTIONS_AMOUNT } from "../constants";
import CategoryCard from "../molecules/CategoryCard";

const CategoricalAnalysis = () => {
  return (
    <Box>
      <Grid container spacing={3} alignItems="center" justifyContent="center">
        <Grid item lg={12}>
          <CategoryCard
            title="Credit Transactions"
            titleType="transactions"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <CategoryCard
            title="Debit Transactions"
            titleType="transactions"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <CategoryCard
            title="Cash Deposits"
            titleType="deposits"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <CategoryCard
            title="Credit Withdrawals"
            titleType="withdrawals"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <CategoryCard
            title="Cheque Issues"
            titleType="issues"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
        <Grid item lg={12}>
          <CategoryCard
            title="Cheque Bounces"
            titleType="bounces"
            chartData={[
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS,
                chartTitle: "Total Credit Transactions",
                legend: "Credit Transactions",
              },
              {
                xAxis: MONTHS,
                yAxis: TRANSACTIONS_AMOUNT,
                chartTitle: "Total Amount Credited",
                legend: "Amount Credited",
              },
            ]}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default CategoricalAnalysis;
