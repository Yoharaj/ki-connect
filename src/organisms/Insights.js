import { Box } from "@mui/material";
import React from "react";
import InsightCard from "../molecules/InsightCard";

const Insights = () => {
  return (
    <Box>
      <InsightCard />
    </Box>
  );
};

export default Insights;
