import React from "react";

import { Box, Container, Typography } from "@mui/material";
import InsightTabs from "../molecules/InsightTabs";
import CategoricalAnalysis from "./CategoricalAnalysis";
import MonthwiseAnalysis from "./MonthwiseAnalysis";
import AnalysisSummary from "../molecules/StatementSummary";
import Insights from "./Insights";
import { updateToastMessage } from "../store/slices/ToastSlice";
import { useDispatch } from "react-redux";

const KiInsight = () => {
  const [value, setValue] = React.useState(0);
  const dispatch = useDispatch();

  const handleChange = (event, newValue) => {
    setValue(newValue);
    event.preventDefault();
  };

  React.useEffect(() => {
    dispatch(
      updateToastMessage({
        message: "",
        severity: "",
      })
    );
  }, []);

  const renderTab = () => {
    switch (value) {
      case 0:
        return <MonthwiseAnalysis />;
      case 1:
        return <CategoricalAnalysis />;
      default:
        return <></>;
    }
  };
  return (
    <Box sx={{ paddingLeft: 5, paddingRight: 5 }}>
      <br />
      <AnalysisSummary />
      <br />
      <Insights />
      <br />
      <InsightTabs value={value} handleChange={handleChange} />
      <br />
      {renderTab()}
    </Box>
  );
};

export default KiInsight;
