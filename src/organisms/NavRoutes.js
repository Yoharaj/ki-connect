import React from "react";
import { Route, Routes } from "react-router-dom";
import KiInsight from "./KiInsight";
import BcPartnerDetails from "./ki-connect/BcPartnerDetails";
import BcPartnerOverview from "./ki-connect/BcPartnerOverview";
import { MOCK_BC_PARTNERS, MOCK_POOLS } from "../Mock";
import Pool from "./ki-connect/Pool";
import PoolDetails from "./ki-connect/PoolDetails";
import DAOverview from "./ki-connect/DAOverview";

const NavRoutes = () => {
  return (
    <Routes>
      <Route
        path="/"
        exact
        element={
          <BcPartnerOverview
            sectionTitle="BC Partner"
            title="BC Partners"
            data={MOCK_BC_PARTNERS}
            isBcPartner={true}
          />
        }
      />
      <Route
        path="/da"
        exact
        element={
          <DAOverview
            title="Direct Assignment"
            sectionTitle="Pool"
            data={MOCK_POOLS}
          />
        }
      />
      <Route path="/bcPartner" exact element={<BcPartnerDetails />} />

      <Route path="/pool" exact element={<PoolDetails />} />
      <Route path="/reports" exact element={<KiInsight />} />
    </Routes>
  );
};

export default NavRoutes;
