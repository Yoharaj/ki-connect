import { Box, Container, Stack, Typography } from "@mui/material";
import React from "react";
import logo from "../assets/logo.svg";
const Landing = ({ updateGuestLogin = () => {} }) => {
  return (
    <Stack
      sx={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <Box
        sx={{
          height: "45vh",
          width: "45vw",
        }}
      >
        <img
          src={logo}
          alt="Ki Connect"
          style={{ height: "100%", width: "100%" }}
        />
      </Box>
      <Box sx={{ display: "flex", height: "100vh" }}>
        <Typography
          align="center"
          variant="h5"
          fontWeight={600}
          sx={{cursor:'pointer'}}
          color="primary"
          onClick={() => updateGuestLogin(true)}
        >
          Login as Guest
        </Typography>
      </Box>
    </Stack>
  );
};
export default Landing;
