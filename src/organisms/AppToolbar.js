import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import StarsIcon from "@mui/icons-material/Stars";
import { AppBar, Box, Button, Stack, Toolbar, Typography } from "@mui/material";
import React from "react";
import logo from "../assets/logo.svg";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { updateKiconnectNavItems } from "../store/slices/KiConnectNavbarSlice";
import { updateLoginState } from "../store/slices/LoginSlice";
import { get } from "lodash";

const AppToolbar = () => {
  const dispatch = useDispatch();

  const user = useSelector(({ login }) => login);

  const handleTabChange = (tabValue = 0) => {
    dispatch(
      updateKiconnectNavItems({
        tabValue,
      })
    );
  };

  const loginUser = () => {
    dispatch(
      updateLoginState({
        openLogin: true,
      })
    );
  };

  const subscribeComponent = (
    <Button color="secondary" onClick={loginUser}>
      <StarsIcon />
      &nbsp;
      <Typography>Subscribe</Typography>
    </Button>
  );
  const userComponent = (
    <Button color="primary">
      <AccountCircleIcon />
      &nbsp;
      <Typography>{get(user, "name", "Lender")}</Typography>
    </Button>
  );
  return (
    <AppBar color="transparent" sx={{ background: "#fff" }}>
      <Toolbar>
        <Stack
          justifyContent="space-between"
          direction="row"
          alignItems="center"
          width="100%"
        >
          <Link to="/">
            <Box sx={{ height: "7vh" }}>
              <img
                src={logo}
                alt="Ki-insights"
                style={{ height: "100%", width: "100%" }}
              />
            </Box>
          </Link>
          <Stack spacing={3} direction={"row"}>
            <Link to="/">
              <Button color="primary" onClick={() => handleTabChange(0)}>
                <Typography fontWeight={600}>Lending</Typography>
              </Button>
            </Link>
            <Link to="/da">
              <Button color="primary" onClick={() => handleTabChange(1)}>
                <Typography fontWeight={600}>Direct Assignments</Typography>
              </Button>
            </Link>
            {get(user, "loggedIn", false) ? userComponent : subscribeComponent}
          </Stack>
        </Stack>
      </Toolbar>
    </AppBar>
  );
};

export default AppToolbar;
