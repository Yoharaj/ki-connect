import { configureStore } from "@reduxjs/toolkit";
import ToastSlice from "./slices/ToastSlice";
import LoaderSlice from "./slices/LoaderSlice";
import ReportSlice from "./slices/ReportSlice";
import MenuDrawerSlice from "./slices/MenuDrawerSlice";
import FilterSlice from "./slices/FilterSlice";
import KiConnectNavbarSlice from "./slices/KiConnectNavbarSlice";
import LoginSlice from "./slices/LoginSlice";
import AlertsSlice from "./slices/AlertsSlice";
import DataSlice from "./slices/DataSlice";

export const store = configureStore({
  reducer: {
    report: ReportSlice,
    loader: LoaderSlice,
    toast: ToastSlice,
    drawer: MenuDrawerSlice,
    filters: FilterSlice,
    navbar:KiConnectNavbarSlice,
    login:LoginSlice,
    alerts:AlertsSlice,
    data:DataSlice
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
