import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {};

export const FilterSlice = createSlice({
  name: "loader",
  initialState: DEFATULT_STATE,
  reducers: {
    updateFilter: (state, action) => ({
      ...state,
      ...action.payload,
    }),
    removeFilter: (state) => (state = {}),
  },
});

export const { updateFilter,removeFilter } = FilterSlice.actions;

export default FilterSlice.reducer;
