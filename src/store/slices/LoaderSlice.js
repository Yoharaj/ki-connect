import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {
  load: false,
  loaderText: "",
};

export const LoaderSlice = createSlice({
  name: "loader",
  initialState: DEFATULT_STATE,
  reducers: {
    updateLoader: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateLoader } = LoaderSlice.actions;

export default LoaderSlice.reducer;
