import { createSlice } from "@reduxjs/toolkit";

const DEFAULT_STATE = {
  message: "",
  severity: "info",
};

export const ToastSlice = createSlice({
  name: "alert",
  initialState: DEFAULT_STATE,
  reducers: {
    updateToastMessage: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateToastMessage } = ToastSlice.actions;

export default ToastSlice.reducer;
