import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {
  open: false,
  list: [],
};

export const MenuDrawerSlice = createSlice({
  name: "drawer",
  initialState: DEFATULT_STATE,
  reducers: {
    updateDrawer: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateDrawer } = MenuDrawerSlice.actions;

export default MenuDrawerSlice.reducer;
