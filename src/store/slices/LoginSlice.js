import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {
  loggedIn: false,
  email: "",
  password: "",
  name: "",
  openLogin: false,
};

export const LoginSlice = createSlice({
  name: "login",
  initialState: DEFATULT_STATE,
  reducers: {
    updateLoginState: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateLoginState } = LoginSlice.actions;

export default LoginSlice.reducer;
