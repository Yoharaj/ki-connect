import { createSlice } from "@reduxjs/toolkit";

const DEFAULT_STATE = {
  tabValue: 0,
};

export const KiConnectNavbarSlice = createSlice({
  name: "navbar",
  initialState: DEFAULT_STATE,
  reducers: {
    updateKiconnectNavItems: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateKiconnectNavItems } = KiConnectNavbarSlice.actions;

export default KiConnectNavbarSlice.reducer;
