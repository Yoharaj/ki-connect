import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {};

export const DataSlice = createSlice({
  name: "data",
  initialState: DEFATULT_STATE,
  reducers: {
    updataData: (state, action) => ({
      ...state,
      ...action.payload,
    }),
    removeData: (state) => (state = {}),
  },
});

export const { updateData,removeData } = DataSlice.actions;

export default DataSlice.reducer;
