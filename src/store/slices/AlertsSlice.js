import { createSlice } from "@reduxjs/toolkit";

const DEFATULT_STATE = {
  open: false,
  message: "",
  severity: "",
};

export const AlertsSlice = createSlice({
  name: "alerts",
  initialState: DEFATULT_STATE,
  reducers: {
    updateAlert: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateAlert } = AlertsSlice.actions;

export default AlertsSlice.reducer;
