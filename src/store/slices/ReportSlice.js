import { createSlice } from "@reduxjs/toolkit";

const DEFAULT_STATE = {
  data: [],
};

export const ReportSlice = createSlice({
  name: "report",
  initialState: DEFAULT_STATE,
  reducers: {
    updateReportData: (state, action) => ({
      ...state,
      ...action.payload,
    }),
  },
});

export const { updateReportData } = ReportSlice.actions;

export default ReportSlice.reducer;
