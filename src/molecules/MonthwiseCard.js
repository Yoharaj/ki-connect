import { Grid, Paper, Stack, Typography } from "@mui/material";
import { get } from "lodash";
import React from "react";
import { useSelector } from "react-redux";
import CardTypoGridTitle from "../atoms/CardTypoGridItem";
import MonthCardCategoryItem from "../atoms/MonthCardCategoryItem";
import BarCharts from "./BarCharts";

const MonthwiseCard = ({
  title = "",
  keyValue = "",
  titleType = "",
  chartData = [],
}) => {
  const currentMonthwiseData = useSelector(({ report }) =>
    get(report, `monthwiseData.${keyValue}`)
  );
  return (
    <Paper elevation={3} sx={{ padding: 2, borderRadius: 4 }}>
      <Typography variant="h6" fontWeight={700}>
        {title}
      </Typography>
      <hr />
      <Grid container spacing={5}>
        <Grid item lg={12}>
          <Stack
            justifyContent="center"
            direction="row"
            width="100%"
            spacing={10}
          >
            <CardTypoGridTitle
              label={`Min EOD Balance`}
              value={
                <>
                  &#8377; {get(currentMonthwiseData, "eod.minEODBalance", "--")}
                </>
              }
            />
            <CardTypoGridTitle
              label={`Max EOD Balance`}
              value={
                <>
                  &#8377; {get(currentMonthwiseData, "eod.maxEODBalance", "--")}
                </>
              }
            />
            <CardTypoGridTitle
              label={`AVG EOD Balance`}
              value={
                <>
                  &#8377;{" "}
                  {get(currentMonthwiseData, "eod.averageEODBalance", "--")}
                </>
              }
            />
          </Stack>
        </Grid>
        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Credit Transactions"
            label={get(
              currentMonthwiseData,
              "credit.noOfCreditTransactions",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "credit.amountOfCreditTransactions",
              "---"
            )}
          />
        </Grid>

        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Debit Transactions"
            label={get(
              currentMonthwiseData,
              "debit.noOfDebitTransactions",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "debit.amountOfDebitTransactions",
              "---"
            )}
          />
        </Grid>

        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Cash Deposits"
            label={get(
              currentMonthwiseData,
              "cashDeposits.noOfCashDeposits",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "cashDeposits.amountOfCashDeposits",
              "---"
            )}
          />
        </Grid>

        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Cash Withdrawals"
            label={get(
              currentMonthwiseData,
              "cashWithdrawls.noOfCashWithdrawals",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "cashWithdrawls.amountOfCashWithdrawals",
              "---"
            )}
          />
        </Grid>

        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Cheque Issues"
            label={get(
              currentMonthwiseData,
              "chequeIssues.noOfChequeIssues",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "chequeIssues.amountOfChequeIssues",
              "---"
            )}
          />
        </Grid>

        <Grid item lg={4}>
          <MonthCardCategoryItem
            title="Cheque Bounces"
            label={get(
              currentMonthwiseData,
              "chequeBounces.noOfChequeBounces",
              "---"
            )}
            value={get(
              currentMonthwiseData,
              "chequeBounces.amountOfChequeBounces",
              "---"
            )}
          />
        </Grid>
        <Grid item lg={12}>
          <Stack
            justifyContent="center"
            direction="row"
            width="100%"
            spacing={3}
          >
            <CardTypoGridTitle
              fontWeight={700}
              label={`Total ${titleType}`}
              value="2"
            />
            <CardTypoGridTitle
              label={`Total Amount`}
              value={<>1,000&#8377;</>}
            />
          </Stack>
        </Grid>
        <Grid item lg={12}>
          <BarCharts {...chartData[0]} />
        </Grid>
      </Grid>
    </Paper>
  );
};

export default MonthwiseCard;
