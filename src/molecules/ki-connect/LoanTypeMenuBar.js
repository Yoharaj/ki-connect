import { Box, Button, Stack } from "@mui/material";
import React from "react";
import { COLOR_CODES } from "../../constants";

const LoanTypeMenuBar = ({loanTypes=[]}) => {
  
  return (
    <Box>
      <Stack
        direction="row"
        spacing={5}
        justifyContent='self-start'
        sx={{
          margin: "auto",
          backgroundColor: COLOR_CODES.light,
          color: "#fff",
        }}
      >
        {loanTypes.map((loanType) => (
          <Button>{loanType}</Button>
        ))}
      </Stack>
    </Box>
  );
};

export default LoanTypeMenuBar;
