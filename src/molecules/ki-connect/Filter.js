import { Box } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { updateDrawer } from "../../store/slices/MenuDrawerSlice";

const Filter = ({ title = "Filter", list = [], icon = <></> }) => {
  const dispatch = useDispatch();

  const openDrawer = () => {
    dispatch(
      updateDrawer({
        open: true,
        list
      })
    );
  };
  return (
    <Box>
      <Box
        sx={{ display: "flex", gap: "10px", cursor: "pointer" }}
        onClick={openDrawer}
      >
        {icon ? icon : <></>} {title}
      </Box>
    </Box>
  );
};

export default Filter;
