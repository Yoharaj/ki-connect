import { get } from "lodash";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ProgressDialog from "../atoms/ProgressDialog";
import ToastMessage from "../atoms/ToastMessage";
import CustomSnackbar from "../atoms/ki-connect/CustomSnackbar";
import MenuDrawer from "../atoms/ki-connect/MenuDrawer";
import Login from "../organisms/ki-connect/Login";
import { updateDrawer } from "../store/slices/MenuDrawerSlice";

const Overlays = () => {
  const isDrawerOpen = useSelector(({ drawer }) => get(drawer, `open`, false));

  const dispatch = useDispatch();

  const handleDrawerClose = () => {
    dispatch(
      updateDrawer({
        open: false,
      })
    );
  };
  return (
    <React.Fragment>
      <ProgressDialog />
      <ToastMessage />
      <MenuDrawer isOpen={isDrawerOpen} onClose={handleDrawerClose} />
      <Login />
      <CustomSnackbar />
    </React.Fragment>
  );
};

export default Overlays;
