import { Box } from "@mui/material";
import React from "react";
import Chart from "chart.js/auto";
import { Bar } from "react-chartjs-2";

const BarCharts = ({ chartTitle = "", xAxis = [], yAxis = [] }) => {
  return (
    <Box
      sx={{
        height: "70vh",
        width: "90vw",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Bar
        datasetIdKey="id"
        options={{
          plugins: {
            title: {
              display: true,
              text: chartTitle,
            },
          },
        }}
        data={{
          labels: xAxis,
          datasets: yAxis,
        }}
      />
    </Box>
  );
};
export default BarCharts;
