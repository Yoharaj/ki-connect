import { Tab, Tabs } from "@mui/material";
import React from "react";

const InsightTabs = ({ value = 0, handleChange = () => {} }) => {
  return (
    <Tabs value={value} onChange={handleChange} centered>
      <Tab label="Month-wise Analysis" />
      <Tab label="Categorical Analysis" />
    </Tabs>
  );
};

export default InsightTabs;
