import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import SavingsIcon from "@mui/icons-material/Savings";
import EmailIcon from "@mui/icons-material/Email";
import HomeIcon from "@mui/icons-material/Home";
import LocalPhoneOutlined from "@mui/icons-material/LocalPhoneOutlined";
import { Box, Grid, Paper, Stack, Typography } from "@mui/material";
import React from "react";
import IconTypography from "../atoms/IconTypography";

const AnalysisSummary = () => {
  const name = "R Yoharaj";
  const address = //getFormattedAddress(
    "NO 13 6TH STREET MAHALAKSHMI NAGAR MADIPAKKAM CHENNAI 600091 TAMIL NADU INDIA";
  //);
  const email = "ajyoga18@gmail.com";
  const pan = "ABCVD1121P";
  const mobileNumber = "9191919191";
  const bankName = "HDFC";
  const accountNumber = "123123123123123";
  const accountType = "Savings";
  return (
    <Box sx={{ margin: "0 auto", width: "50vw" }}>
      <Paper elevation={3} sx={{ padding: 2, borderRadius:4 }}>
        <Grid container spacing={1}>
          <Grid item lg={12}>
            <Typography variant="h6" fontWeight={700}>
              {name}
            </Typography>
          </Grid>
          <Grid item lg={6}>
            <Stack margin="0 auto" justifyContent="center" spacing={2}>
              <IconTypography
                icon={<AccountBalanceIcon />}
                label={accountNumber}
              />
              <IconTypography icon={<CreditCardIcon />} label={pan} />
              <IconTypography icon={<SavingsIcon />} label={accountType} />
            </Stack>
          </Grid>
          <Grid item lg={6}>
            <Stack justifyContent="center" spacing={2}>
              <IconTypography
                label={mobileNumber}
                icon={<LocalPhoneOutlined fontSize="small" />}
              />
              <IconTypography
                label={email}
                icon={<EmailIcon fontSize="small" />}
              />
              <IconTypography
                label={address}
                icon={<HomeIcon fontSize="small" />}
              />
            </Stack>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export default AnalysisSummary;
