import { Box } from "@mui/material";
import React from "react";
import Chart from "chart.js/auto";
import { Line } from "react-chartjs-2";

const LineCharts = ({ chartTitle="" ,legend="", xAxis=[], yAxis=[]}) => {
  return (
    <Box sx={{ height: "40vh", width: "45vw" }}>
      <Line
        datasetIdKey="id"
        options={{
          plugins: {
            title: {
              display: true,
              text: chartTitle,
            },
          },
        }}
        data={{
          labels: xAxis,

          datasets: [
            {
              id: 1,
              label: legend,
              data: yAxis,
            },
          ],
        }}
      />
    </Box>
  );
};
export default LineCharts;
