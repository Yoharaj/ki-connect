import { Grid, Paper, Stack, Typography } from "@mui/material";
import React from "react";
import { MONTHS, TRANSACTIONS, TRANSACTIONS_AMOUNT } from "../constants";
import LineCharts from "./LineCharts";
import CardTypoGridTitle from "../atoms/CardTypoGridItem";
import CategoryItemByMonthTable from "../atoms/CategoryItemByMonthTable";

const CategoryCard = ({ title = "", titleType = "", chartData = [] }) => {
  return (
    <Paper elevation={3} sx={{ padding: 2, borderRadius: 4 }}>
      <Typography variant="h6" fontWeight={700}>
        {title}
      </Typography>
      <hr />
      <Grid container spacing={2}>
        <Grid item lg={12}>
          <Stack
            justifyContent="center"
            direction="row"
            width="100%"
            spacing={10}
          >
            <CardTypoGridTitle
              fontWeight={700}
              label={`Total ${titleType}`}
              value={TRANSACTIONS.reduce((a,b) => a+b,0)}
            />
            <CardTypoGridTitle
              label={`Total Amount`}
              value={<>&#8377; {TRANSACTIONS_AMOUNT.reduce((a,b) => a+b,0)}</>}
              
            />
          </Stack>
        </Grid>
        <Grid item lg={12}>
          <CategoryItemByMonthTable />
        </Grid>
        <Grid item lg={12}>
          <Stack direction="row">
            <LineCharts {...chartData[0]} />
            <LineCharts {...chartData[1]} />
          </Stack>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default CategoryCard;
