import { Grid, Paper, Stack, Typography } from "@mui/material";
import React from "react";
import { MOCK_Y_AXIS, MONTHS } from "../constants";
import { DS_DATA } from "../constants/MockData";
import LineCharts from "./LineCharts";

const InsightCard = () => {
  const allPenaltyData = DS_DATA.map((a) => a.penalty);
  const allAvgIERatio = DS_DATA.map((a) => a.ie_ratio);
  const allDiRatio = DS_DATA.map((a) => a.di_ratio);
  const allDeRatio = DS_DATA.map((a) => a.de_ratio);

  let emi_eligibility = DS_DATA.reduce((a, b) => a + b.emi_eligibility, 0);
  emi_eligibility = emi_eligibility / DS_DATA.length;
  emi_eligibility = emi_eligibility + "";
  emi_eligibility =
    emi_eligibility.length > 3
      ? emi_eligibility.slice(0, 2) + "K"
      : emi_eligibility;

  const financialHealth = DS_DATA.reduce((a, b) => a + b.fin_health, 0);

  const averageDebtIncomeRatio = DS_DATA.reduce((a, b) => a + b.di_ratio, 0);

  const averageDebtExpenseRatio = DS_DATA.reduce((a, b) => a + b.de_ratio, 0);
  const averageIncomeRatio = DS_DATA.reduce((a, b) => {
    console.log(a, b, "resp");
    return a + b.ie_ratio;
  }, 0);
  return (
    <Paper elevation={5} sx={{ padding: 2, borderRadius: 4 }}>
      <Stack spacing={3}>
        <Typography fontWeight={700} align="center" variant="h5">
          Customer Financial Stability
        </Typography>
        {/* <Stack
          direction="row"
          alignItems="center"
          justifyContent="end"
          sx={{ zIndex: 999, position:'relative'}}
        >
          <Paper elevation={4} sx={{ borderRadius:4, padding:4}}>
            <Stack>
              <Typography variant="h6">Financial Health </Typography>
              <Typography>{`0 < score < 35 `} - High Risk</Typography>
              <Typography>{`35 < score < 70 `} - Medium Risk</Typography>
              <Typography>{`0 < score < 30 `} - Low Risk</Typography>
            </Stack>
          </Paper>
        </Stack> */}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-evenly"
        >
          <Paper elevation={3} sx={{ padding: 3 }}>
            <Typography variant="h6" fontWeight={700}>
              Financial Health -{" "}
              <b>{Math.round(financialHealth / DS_DATA.length)}</b>
            </Typography>
          </Paper>
          <Paper elevation={3} sx={{ padding: 3 }}>
            <Typography variant="h6" fontWeight={700}>
              EMI Eligibility - &#8377; {emi_eligibility}
            </Typography>
          </Paper>
        </Stack>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-evenly"
          spacing={3}
        >
          <Typography variant="body" fontWeight={700}>
            Average Income-Expense Ratio -{" "}
            <b>
              {
                Math.round((averageIncomeRatio / DS_DATA.length) * 100) / 100
                // (num + Number.EPSILON) * 100) / 100x
              }
            </b>
          </Typography>
          <Typography variant="body" fontWeight={700}>
            Average Debt-Expense Ratio -{" "}
            <b>
              {Math.round((averageDebtExpenseRatio / DS_DATA.length) * 100) /
                100}
            </b>
          </Typography>
          <Typography variant="body" fontWeight={700}>
            Average Debt-Income Ratio -{" "}
            <b>
              {Math.round((averageDebtIncomeRatio / DS_DATA.length) * 100) /
                100}
            </b>
          </Typography>
        </Stack>
        <Grid container spacing={4}>
          <Grid item lg={6}>
            <LineCharts
              xAxis={MONTHS}
              yAxis={allAvgIERatio}
              chartTitle={"Average Income-Expense Ratio"}
              legend={"Income - Expense"}
            />
          </Grid>
          <Grid item lg={6}>
            <LineCharts
              xAxis={MONTHS}
              yAxis={allDiRatio}
              chartTitle={"Average Debit-Income Ratio"}
              legend={"Debit - Income"}
            />
          </Grid>

          <Grid item lg={6}>
            <LineCharts
              xAxis={MONTHS}
              yAxis={allPenaltyData}
              chartTitle={"Penalty Over Time"}
              legend="Penalty"
            />
          </Grid>
          <Grid item lg={6}>
            <LineCharts
              xAxis={MONTHS}
              yAxis={allDeRatio}
              chartTitle={"All Debit-Expense Ratio"}
              legend="Debit- Expense"
            />
          </Grid>
        </Grid>
      </Stack>
    </Paper>
  );
};
export default InsightCard;
