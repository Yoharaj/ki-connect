import { Alert, Snackbar } from "@mui/material";
import { isEmpty } from "lodash";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateLoader } from "../store/slices/LoaderSlice";

const ToastMessage = () => {
    const dispatch = useDispatch();
  const { message, severity } = useSelector(({ toast }) => toast);

  const handleClose = () => {
    dispatch(
      updateLoader({
        message: "",
        severity:''
      })
    );
  };
  return message && (
    <Snackbar
      open={!isEmpty(message)}
      autoHideDuration={4000}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      onClose={handleClose}
    >
      <Alert
        variant="filled"
        elevation={7}
        severity={severity}
        sx={{ width: "100%" }}
        onClose={handleClose}
      >
        {message}
      </Alert>
    </Snackbar>
  );
};

export default ToastMessage;
