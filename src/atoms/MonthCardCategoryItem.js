import { Paper, Stack, Typography } from "@mui/material";
import React from "react";

const MonthCardCategoryItem = ({ title = "", label = "", value = "" }) => {
  return (
    <Paper elevation={8} sx={{ padding: 1, borderRadius: 3 }}>
      <Typography variant="h6"> {title}</Typography>
      <hr />
      <Stack direction="row" justifyContent="space-evenly">
        <Stack>
          <Typography variant="body"> Total</Typography>
          <Typography variant="h6" align="center" fontWeight={700}>
            {label}
          </Typography>
        </Stack>
        <Stack>
          <Typography variant="body">Total Amount</Typography>
          <Typography variant="h6" align="center" fontWeight={700}>
           {value}
          </Typography>
        </Stack>
      </Stack>
    </Paper>
  );
};

export default MonthCardCategoryItem;
