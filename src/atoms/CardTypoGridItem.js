import { Stack, Typography } from "@mui/material";
import React from "react";

const CardTypoGridTitle = ({ value = <></>, label = "" }) => {
  return (
      <Typography fontWeight={700} variant="body">{label}  - {value}</Typography>
  );
};

export default CardTypoGridTitle;
