import { Stack, Typography } from "@mui/material";
import React from "react";

const IconTypography = ({ icon = <></>, label = "" }) => {
  return (
    <Stack direction='row' spacing={1}alignItems='center'>
      {icon}
      <Typography variant="caption">{label}</Typography>
    </Stack>
  );
};

export default IconTypography;