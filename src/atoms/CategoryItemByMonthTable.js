import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React from "react";
import { MONTHS, TRANSACTIONS, TRANSACTIONS_AMOUNT } from "../constants";

const CategoryItemByMonthTable = () => {
  return (
    <Table stickyHeader aria-label="sticky table">
      <TableHead>
        <TableRow>
          <TableCell></TableCell>
          {MONTHS.map((month, index) => (
            <TableCell key={index}>
              <Typography fontWeight={700}>{month}-22</Typography>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell key={12}>
            <Typography fontWeight={700}>Transactions</Typography>
          </TableCell>
          {MONTHS.map((month, index) => (
            <TableCell key={index}>{TRANSACTIONS[index]}</TableCell>
          ))}
        </TableRow>
        <TableRow>
          <TableCell key={12}>
            <Typography fontWeight={700}>Amount</Typography>
          </TableCell>
          {MONTHS.map((month, index) => (
            <TableCell key={index}>{TRANSACTIONS_AMOUNT[index]}</TableCell>
          ))}
        </TableRow>
      </TableBody>
    </Table>
  );
};

export default CategoryItemByMonthTable;
