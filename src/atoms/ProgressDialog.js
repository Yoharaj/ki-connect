import {
  CircularProgress,
  Dialog,
  DialogContent,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";

const ProgressDialog = () => {
  const { loaderText, load } = useSelector(({ loader }) => loader);

  return (
    <Dialog open={load}>
      <DialogContent>
        <Stack alignItems="center" justifyContent="center" spacing={2}>
          <CircularProgress />
          <Typography>{loaderText}</Typography>
        </Stack>
      </DialogContent>
    </Dialog>
  );
};

export default ProgressDialog;
