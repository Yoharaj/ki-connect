import CloseIcon from "@mui/icons-material/Close";
import React from "react";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import { IconButton } from "@mui/material";
import { useDispatch } from "react-redux";
import { removeFilter, updateFilter } from "../../store/slices/FilterSlice";

const SingleSelect = ({ options, onSelect, parameter = "", label = "" }) => {
  const dispatch = useDispatch();

  const handleChange = (event) => {
    dispatch(updateFilter({ [parameter]: event.target.value }));
  };

  const handleRemoveFilter = (event) => {
    event.stopPropagation();
    dispatch(removeFilter());
  };

  return (
    <FormControl fullWidth>
      <InputLabel id="demo-select-small-label">{label}</InputLabel>
      <Select
        labelId="select-label"
        id="select"
        onChange={handleChange}
        size="small"
        label={label}
        color="primary"
        renderValue={(selected) => (
            <div style={{ display: "flex", alignItems: "center" }} 
                onClick={handleRemoveFilter}
                >
            <div style={{ flex: 1 }}>{selected}</div>
            {selected && (
              <IconButton
                aria-label="clear selection"
              >
                <CloseIcon fontSize="small" size="small" />
              </IconButton>
            )}
          </div>
        )}
      >
        {options.map((option, index) => (
          <MenuItem key={index} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SingleSelect;
