import React from "react";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";

const RupeesDisplay = ({ amount }) => {
  // Format amount as INR
  const formattedAmount = new Intl.NumberFormat("en-IN", {
    style: "currency",
    currency: "INR",
    minimumFractionDigits: 0,
  }).format(amount);

  return (
    <Box>
      <Typography>Average Ticket Size</Typography>
      <Typography fontWeight={600}>{formattedAmount}</Typography>
    </Box>
  );
};

export default RupeesDisplay;
