import React from "react";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { useDispatch, useSelector } from "react-redux";
import { get } from "lodash";
import { updateAlert } from "../../store/slices/AlertsSlice";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CustomSnackbar = () => {
  const alert = useSelector(({ alerts }) => alerts);
  const dispatch = useDispatch();

  const onClose = () => {
    dispatch(
      updateAlert({
        open: false,
        alertText: "",
        severity: "",
      })
    );
  };
  return (
    <Snackbar
      open={get(alert, "open", false)}
      autoHideDuration={2000}
      onClose={onClose}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
    >
      <Alert onClose={onClose} severity={get(alert, "severity", "")}>
        {get(alert, "message", "")}
      </Alert>
    </Snackbar>
  );
};

export default CustomSnackbar;
