// DrawerComponent.js
import React from "react";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import { Box, Button, Container, Stack, Typography } from "@mui/material";
import SingleSelect from "./SingleSelect";

const MenuDrawer = ({ isOpen = false, onClose = () => {} }) => {
  const [filters, selectedFilters] = React.useState([]);

  const onFilterSelection = (value = {}) => {
    filters.push(value);
  };

  return (
    <Drawer anchor="right" open={isOpen} onClose={onClose}>
      <Container sx={{ width: 350, paddingTop:5 }}>
        <Typography variant="h6">Filter by</Typography>
        <List>
          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="gender"
                options={["Male", "Female", "Others"]}
                onSelect={onFilterSelection}
                label="Gender"
              />
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="location"
                options={["Gujarat", "Tamil Nadu", "Kerala"]}
                onSelect={onFilterSelection}
                label="Location"
              />
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="loanAmount"
                options={["50k - 100k", "100k - 5L", "5L - more"]}
                onSelect={onFilterSelection}
                label="Loan Amount"
              />
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="loanPSLClassification"
                options={[
                  "Small Artisians",
                  "Small and Marginal farmer",
                  "NRLM Benificiary",
                  "SC and ST tribe",
                  "Distressed farmer",
                  "Export credit",
                  "Minority community",
                  "Person with disability",
                ]}
                onSelect={onFilterSelection}
                label="Loan PSL Classification"
              />
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="subSectorCode"
                options={["Dairy", "Milk", "5Star"]}
                onSelect={onFilterSelection}
                label="Sub-sector Code"
              />
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <SingleSelect
                parameter="cityType"
                options={["T30", "T123"]}
                onSelect={onFilterSelection}
                label="City Type"
              />
            </ListItemText>
          </ListItem>
        </List>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" color="primary" onClick={onClose}>
            Apply Filters
          </Button>
        </Box>
      </Container>
    </Drawer>
  );
};

export default MenuDrawer;
