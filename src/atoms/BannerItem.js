import { Stack, Typography } from "@mui/material";
import React from "react";

const BannerItem = ({ label = "", value = "" }) => {
  return (
    <Stack direction='row'>
      <Typography variant="h6">{label ? label+" - ": "---"}</Typography>
      <Typography variant="h6" fontWeight={700}>{value}</Typography>
    </Stack>
  );
};

export default BannerItem;
