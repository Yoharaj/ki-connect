import { Stack } from "@mui/material";
import React from "react";
import BannerItem from "./BannerItem";

const CategoryAnalysisBanner = () => {
  return (
    <Stack spacing={5} direction="row" justifyContent="center" padding={2}>
      <BannerItem label="Avg EOD Balance" value={<>&#8377; 1,26,990</>} />
      <BannerItem label="Max EOD Balance" value={<>&#8377; 1,26,990</>} />
      <BannerItem label="Min EOD Balance" value={<>&#8377; 1,26,990</>} />
    </Stack>
  );
};

export default CategoryAnalysisBanner;
