import React from "react";
import Overlays from "./molecules/Overlays";
import AppToolbar from "./organisms/AppToolbar";
import NavRoutes from "./organisms/NavRoutes";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Box, Container } from "@mui/material";
import Landing from "./organisms/Landing";

ChartJS.register(ArcElement, Tooltip, Legend);

const App = () => {
  const [loginAsGuest, setLoginAsGuest] = React.useState(false);

  const kiConnect = (
    <React.Fragment>
      <AppToolbar />
      <Box sx={{ marginTop: "15vh" }}>
        <Container>
          <NavRoutes />
        </Container>
      </Box>
      <Overlays />
    </React.Fragment>
  );
  return <Box>{loginAsGuest ? kiConnect : <Landing updateGuestLogin={setLoginAsGuest}/>}</Box>;
};

export default App;
