export const MOCK_DATA = {
  "2022-04": {
    noOfCreditTransactions: 86,
    amountOfCreditTransactions: 69777.0,
    noOfDebitTransactions: 87,
    amountOfDebitTransactions: 56089.42,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 4,
    amountOfCashWithdrawals: 10500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 174177.88,
    maxEODBalance: 240547.88,
    averageEODBalance: 207362.88,
  },
  "2022-01": {
    noOfCreditTransactions: 87,
    amountOfCreditTransactions: 70330.0,
    noOfDebitTransactions: 87,
    amountOfDebitTransactions: 94865.55,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 5,
    amountOfCashWithdrawals: 5500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 127864.41,
    maxEODBalance: 214016.46,
    averageEODBalance: 170940.435,
  },
  "2022-02": {
    noOfCreditTransactions: 68,
    amountOfCreditTransactions: 64425.0,
    noOfDebitTransactions: 69,
    amountOfDebitTransactions: 61926.52,
    noOfCashDeposits: 2,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 2,
    amountOfCashWithdrawals: 3000.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 129059.39,
    maxEODBalance: 193049.39,
    averageEODBalance: 161054.39,
  },
  "2022-03": {
    noOfCreditTransactions: 101,
    amountOfCreditTransactions: 114531.0,
    noOfDebitTransactions: 102,
    amountOfDebitTransactions: 87712.09,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 2,
    amountOfCashWithdrawals: 2000.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 139685.3,
    maxEODBalance: 218936.3,
    averageEODBalance: 179310.8,
  },
  "2022-05": {
    noOfCreditTransactions: 131,
    amountOfCreditTransactions: 83699.2,
    noOfDebitTransactions: 132,
    amountOfDebitTransactions: 125346.12,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 7,
    amountOfCashWithdrawals: 9100.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 134418.96,
    maxEODBalance: 231785.88,
    averageEODBalance: 183102.42,
  },
  "2022-06": {
    noOfCreditTransactions: 84,
    amountOfCreditTransactions: 77945.0,
    noOfDebitTransactions: 83,
    amountOfDebitTransactions: 44464.35,
    noOfCashDeposits: 1,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 2,
    amountOfCashWithdrawals: 3500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 152961.61,
    maxEODBalance: 224319.61,
    averageEODBalance: 188640.61,
  },
  "2022-07": {
    noOfCreditTransactions: 113,
    amountOfCreditTransactions: 72955.0,
    noOfDebitTransactions: 112,
    amountOfDebitTransactions: 66926.69,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 5,
    amountOfCashWithdrawals: 8500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 172032.92,
    maxEODBalance: 234437.92,
    averageEODBalance: 203235.42,
  },
  "2022-08": {
    noOfCreditTransactions: 113,
    amountOfCreditTransactions: 102407.0,
    noOfDebitTransactions: 114,
    amountOfDebitTransactions: 102679.98,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 3,
    amountOfCashWithdrawals: 5000.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 163005.34,
    maxEODBalance: 230494.34,
    averageEODBalance: 196749.84,
  },
  "2022-09": {
    noOfCreditTransactions: 120,
    amountOfCreditTransactions: 93754.0,
    noOfDebitTransactions: 121,
    amountOfDebitTransactions: 78790.9,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 4,
    amountOfCashWithdrawals: 7500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 173823.04,
    maxEODBalance: 245138.04,
    averageEODBalance: 209480.54,
  },
  "2022-10": {
    noOfCreditTransactions: 121,
    amountOfCreditTransactions: 141048.0,
    noOfDebitTransactions: 122,
    amountOfDebitTransactions: 159956.67,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 5,
    amountOfCashWithdrawals: 10000.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 135544.06,
    maxEODBalance: 256726.06,
    averageEODBalance: 196135.06,
  },
  "2022-11": {
    noOfCreditTransactions: 123,
    amountOfCreditTransactions: 115619.0,
    noOfDebitTransactions: 124,
    amountOfDebitTransactions: 186895.78,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 3,
    amountOfCashWithdrawals: 4500.0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 46978.59,
    maxEODBalance: 226089.37,
    averageEODBalance: 136533.98,
  },
  "2022-12": {
    noOfCreditTransactions: 99,
    amountOfCreditTransactions: 22030.0,
    noOfDebitTransactions: 100,
    amountOfDebitTransactions: 130677.96,
    noOfCashDeposits: 0,
    amountOfCashDeposits: 0,
    noOfCashWithdrawals: 0,
    amountOfCashWithdrawals: 0,
    noOfChequeDeposits: null,
    amountOfChequeDeposits: null,
    noOfChequeIssues: null,
    amountOfChequeIssues: null,
    noOfInwardChequeBounces: null,
    amountOfInwardChequeBounces: null,
    minEODBalance: 25983.03,
    maxEODBalance: 154787.59,
    averageEODBalance: 90385.31,
  },
};

export const DS_DATA = [
  {
    year: 2022,
    month: 1,
    deposits: 70330.0,
    withdrawals: 101815.65,
    debt: 8000.0,
    penalty: 200.0,
    recc_income: 61900.0,
    ie_ratio: 0.6907582479,
    de_ratio: 0.0785733824,
    di_ratio: 0.1137494668,
    emi_eligibility: 27165.0,
    fin_health: 75,
  },
  {
    year: 2022,
    month: 2,
    deposits: 64425.0,
    withdrawals: 67926.52,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 61990.0,
    ie_ratio: 0.948451356,
    de_ratio: 0.1177743244,
    di_ratio: 0.1241753977,
    emi_eligibility: 24212.5,
    fin_health: 71,
  },
  {
    year: 2022,
    month: 3,
    deposits: 113531.0,
    withdrawals: 95200.89,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 80467.0,
    ie_ratio: 1.1925413722,
    de_ratio: 0.0840328278,
    di_ratio: 0.0704653355,
    emi_eligibility: 48765.5,
    fin_health: 91,
  },
  {
    year: 2022,
    month: 4,
    deposits: 69777.0,
    withdrawals: 59089.42,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 61870.0,
    ie_ratio: 1.1808712964,
    de_ratio: 0.1353880272,
    di_ratio: 0.1146509595,
    emi_eligibility: 26888.5,
    fin_health: 89,
  },
  {
    year: 2022,
    month: 5,
    deposits: 83334.2,
    withdrawals: 134146.12,
    debt: 8000.0,
    penalty: 800.0,
    recc_income: 61870.0,
    ie_ratio: 0.6212196074,
    de_ratio: 0.0596364621,
    di_ratio: 0.0959990016,
    emi_eligibility: 33667.1,
    fin_health: 78,
  },
  {
    year: 2022,
    month: 6,
    deposits: 77865.0,
    withdrawals: 52048.35,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 71693.0,
    ie_ratio: 1.4960128419,
    de_ratio: 0.1537032394,
    di_ratio: 0.1027419251,
    emi_eligibility: 30932.5,
    fin_health: 92,
  },
  {
    year: 2022,
    month: 7,
    deposits: 72895.0,
    withdrawals: 74377.69,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 62405.0,
    ie_ratio: 0.9800653933,
    de_ratio: 0.107559135,
    di_ratio: 0.1097468962,
    emi_eligibility: 28447.5,
    fin_health: 82,
  },
  {
    year: 2022,
    month: 8,
    deposits: 102407.0,
    withdrawals: 109679.98,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 62489.0,
    ie_ratio: 0.9336890835,
    de_ratio: 0.0729394735,
    di_ratio: 0.0781196598,
    emi_eligibility: 43203.5,
    fin_health: 88,
  },
  {
    year: 2022,
    month: 9,
    deposits: 93754.0,
    withdrawals: 87190.9,
    debt: 8000.0,
    penalty: 400.0,
    recc_income: 71480.0,
    ie_ratio: 1.0752727636,
    de_ratio: 0.0917526944,
    di_ratio: 0.0853296926,
    emi_eligibility: 38877.0,
    fin_health: 72,
  },
  {
    year: 2022,
    month: 10,
    deposits: 141048.0,
    withdrawals: 157097.67,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 61900.0,
    ie_ratio: 0.8978363587,
    de_ratio: 0.0509237343,
    di_ratio: 0.0567182803,
    emi_eligibility: 62524.0,
    fin_health: 76,
  },
  {
    year: 2022,
    month: 11,
    deposits: 115619.0,
    withdrawals: 168875.78,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 49202.0,
    ie_ratio: 0.6846393248,
    de_ratio: 0.047372098,
    di_ratio: 0.0691927797,
    emi_eligibility: 49809.5,
    fin_health: 69,
  },
  {
    year: 2022,
    month: 12,
    deposits: 21932.0,
    withdrawals: 138626.96,
    debt: 8000.0,
    penalty: 0.0,
    recc_income: 1132.0,
    ie_ratio: 0.158208764,
    de_ratio: 0.0577088324,
    di_ratio: 0.3647638154,
    emi_eligibility: 2966.0,
    fin_health: 64,
  },
];
