export const COLOR_CODES = { primary: "#0F547E", secondary: "#28B2B6", light:'#28B2B61A' };

export const MONTHS_FULLNAME = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
export const TRANSACTIONS = [2, 3, 2, 1, 4, 3, 1, 2, 4, 5, 7, 7];
export const TRANSACTIONS_AMOUNT = [
  2000, 3000, 2000, 1000, 4000, 3000, 1000, 2500, 4000, 5000, 4000, 5000,
];
export const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const MOCK_Y_AXIS = [1, 2, 1, 2, 2, 1, 3, 3, 1, 4];

export const MOCK_Y_AXIS_AMOUNT = [
  1000, 2000, 3000, 1000, 1200, 1000, 2000, 3000, 1000, 1200,
];

export const MOCK_MONTH_X_AXIS = [
  "Credit",
  "Debit",
  "Deposits",
  "Withdrawals",
  "Cheque Issues",
  "Cheque Bounces",
];

export const MOCK_MONTH_Y_AXIS = [
  {
    label: "Total Transaction",
    backgroundColor: COLOR_CODES.secondary,
    borderColor: COLOR_CODES.secondary,
    borderWidth: 1,
    //stack: 1,
    hoverBackgroundColor: COLOR_CODES.secondary,
    hoverBorderColor: COLOR_CODES.secondary,
    data: [65, 59, 80, 81, 56, 55, 40],
  },

  {
    label: "Total Amount",
    backgroundColor: COLOR_CODES.primary,
    borderColor: COLOR_CODES.primary,
    borderWidth: 1,
    //stack: 1,
    hoverBackgroundColor: COLOR_CODES.primary,
    hoverBorderColor: COLOR_CODES.primary,
    data: [45, 79, 50, 41, 16, 85, 20],
  },
];
